#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "libntru/src/types.h"
#include "libntru/src/ntru_endian.h"
#include "libntru/src/ntru.h"
#include "libntru/src/encparams.h"
#include "libntru/src/key.h"
#include "libntru/src/rand.h"
#include "libntru/src/err.h"

struct NtruEncParams sf_get_params_EES1087EP2(void)
{
    /* struct NtruEncParams params = NTRU_DEFAULT_PARAMS_256_BITS; patented until 8-24-2021 */
    struct NtruEncParams params = EES1087EP2;

    return params;
}
struct NtruEncParams sf_get_params_EES1171EP1(void)
{
    /* struct NtruEncParams params = NTRU_DEFAULT_PARAMS_256_BITS; patented until 8-24-2021 */
    struct NtruEncParams params = EES1171EP1;

    return params;
}
struct NtruEncParams sf_get_params_EES1499EP1(void)
{
    /* struct NtruEncParams params = NTRU_DEFAULT_PARAMS_256_BITS; patented until 8-24-2021 */
    struct NtruEncParams params = EES1499EP1;

    return params;
}
struct NtruEncParams sf_get_params_EES743EP1(void)
{
    /* struct NtruEncParams params = NTRU_DEFAULT_PARAMS_256_BITS; patented until 8-24-2021 */
    struct NtruEncParams params = EES743EP1;

    return params;
}
struct NtruEncParams sf_get_params(void)
{
    return sf_get_params_EES1087EP2();
}

NtruEncKeyPair sf_gen_ntru_key(void)
{
    /* key generation */
    struct NtruEncParams params = sf_get_params(); 
    NtruRandGen rng_def = NTRU_RNG_DEFAULT;
    NtruRandContext rand_ctx_def;
    if (ntru_rand_init(&rand_ctx_def, &rng_def) != NTRU_SUCCESS)
        printf("rng fail\n");
    NtruEncKeyPair kp;
    if (ntru_gen_key_pair(&params, &kp, &rand_ctx_def) != NTRU_SUCCESS)
        printf("keygen fail\n");


    /* release RNG resources */
    if (ntru_rand_release(&rand_ctx_def) != NTRU_SUCCESS)
        printf("rng fail\n");

    return kp;
}

uint8_t sf_ntru_encrypt(uint8_t *msg, NtruEncPubKey *pub, const NtruEncParams *params, uint8_t *enc)
{

    NtruRandGen rng_def = NTRU_RNG_DEFAULT;
    NtruRandContext rand_ctx_def;
    if (ntru_rand_init(&rand_ctx_def, &rng_def) != NTRU_SUCCESS)
        printf("rng fail\n");
    
    uint8_t ret_val = ntru_encrypt(msg, strlen(msg), pub, params, &rand_ctx_def, enc);

    /* release RNG resources */
    if (ntru_rand_release(&rand_ctx_def) != NTRU_SUCCESS)
        printf("rng fail\n");
    
    return ret_val;
}

uint8_t sf_ntru_decrypt(uint8_t *enc, NtruEncKeyPair *kp, const NtruEncParams *params, uint8_t *dec, uint16_t *dec_len) 
{

    return ntru_decrypt(enc, kp, params, dec, dec_len);

}

uint8_t sf_ntru_max_msg_len(const NtruEncParams *params)
{
    return ntru_max_msg_len(params);
}


