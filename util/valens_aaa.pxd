# valens_aaa.pxd

from util.valens_ctypes cimport *
from util.valens_encparams cimport *

from libc.stdint cimport uint8_t, uint16_t, int16_t
cimport cpython.array


cdef extern from "util/aaa.h":
    NtruEncParams sf_get_params_EES1087EP2()
    NtruEncParams sf_get_params_EES1171EP1()
    NtruEncParams sf_get_params_EES1499EP1()
    NtruEncParams sf_get_params_EES743EP1()
    NtruEncParams sf_get_params()
    NtruEncKeyPair sf_gen_ntru_key()
    uint8_t sf_ntru_encrypt(uint8_t* msg, NtruEncPubKey* pub, NtruEncParams* params, uint8_t* enc)
    uint8_t sf_ntru_decrypt(uint8_t* enc, NtruEncKeyPair* kp, NtruEncParams* params, uint8_t* dec, uint16_t* dec_len) 
    uint8_t sf_ntru_max_msg_len(NtruEncParams* params)
